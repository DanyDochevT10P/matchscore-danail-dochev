from dataclasses import Field
from typing import Optional
from pydantic import BaseModel, EmailStr, constr
from datetime import datetime

#########################
# Log In System
class UserLogIn(BaseModel):
    email: Optional[EmailStr] | str | None
    password: Optional[str] = None

class TokenData(BaseModel):
    id: Optional[str] = None
    role: Optional[str] = None

#########################
#Players
PLAYUsername = constr(regex = r'^[a-zA-Z\s\-]+$')

class Player(BaseModel):
    #make regex for onlyh
    full_name: PLAYUsername
    country:str

########################
#Match
class Match():
    # id:int
    def __init__(self,player1:str, player2:str,
                 time_of_play:datetime,match_format:str):
        self.player1 = player1
        self.player2 = player2
        self.time_of_play = time_of_play
        self.match_format = match_format
    # score_t1:int | None
    # score_t2: int | None

class MatchForGetTour():
    # id:int
    def __init__(self,time_of_play:datetime,match_format:str,
                 player1:str, player2:str,
                 score1,score2):
        self.time_of_play = time_of_play
        self.match_format = match_format
        self.player1 = player1
        self.player2 = player2        
        self.score1 = score1
        self.score2 = score2

class Score(BaseModel):
    score1:int
    score2:int

# Tournaments
class CreateTournament(BaseModel):
    title:str
    tournament_format: str
    match_format:str
    participants: tuple 

class Tournament(BaseModel):
    title:str
    tournament_format: str
    match_format:str

class ReturnTournament():
    def __init__(self,title,format,matches):
        self.title=title
        self.format=format
        self.matches=matches

    # matches_format:str
    # players: list

#########################
class User(BaseModel):
    id: int
    email: str
    password: str | None
    role: str #user, director, admin
    associated: list[Player] | None

    @classmethod
    def from_users_query(cls, id,email, password,role, associated):#, password):
        return cls(id=id,email=email,password=password,
                   role=role,associated=associated)#,password=password)
    
class LoginData(BaseModel):
    email: str
    password: str


class CreateUser(BaseModel):
    email: str
    password: str | None

    @classmethod
    def from_users_query(cls, email, associated, password):
        return cls(email=email,associated=associated, password=password)

class DirectorRequest():
    id_dir_request:int
    id_user:int 
    
class PromoteToDirectorRequest(BaseModel):
    id_user:int
    id_request:int
    admin_decision:str


class NextRoundInfo(BaseModel):
    id_t:int
    ids_matches: list
    curr_round:int