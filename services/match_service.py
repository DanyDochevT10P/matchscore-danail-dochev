from fastapi import HTTPException, Response
from data.database import fetchOne_query, read_query, insert_query, update_query,count_query
from fastapi import APIRouter, HTTPException, status, Depends,Response
from data.models import Match,Player

def change_match_score(score1:int, score2:int, id:int):
    
    result = update_query('''UPDATE match_score.match 
                            SET score1=?,score2=?
                            WHERE id_match=?''',(score1, score2,id))

    return score1,score2

def return_all_matches():
    '''
    Returns all played matches sorted by date of play
    '''
    pass


def return_match_by_id(id_match:int):
    '''
    Return a match by id. It's date could be 
    either in the future or in the past
    '''
    user_data = fetchOne_query('''SELECT time_of_play,match_format,player1,player2 
    FROM match_score.match WHERE id_match = ?''', (id_match,))

    return user_data