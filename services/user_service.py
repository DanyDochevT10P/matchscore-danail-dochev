from data.database import fetchOne_query, insert_query, update_query
from pydantic import BaseModel, EmailStr, constr
from data.models import DirectorRequest, User
from mariadb import IntegrityError
from utils.hashing import hash,verify


_SEPARATOR = ';;;token;;;'

def user_by_email(email: str):
    """
    Obtains user by given email
    Args:
        email (str): The email of specific user
    Returns:
        Specific user data or None
    """
    user_data = fetchOne_query('''SELECT id_user,email,password,role 
    FROM user WHERE email = ?''', (email,))

    if user_data is None:
        return None
    
    return user_data

def try_login(email: str, password: str) -> User | None:

    user_data = user_by_email(email)
    return user_data if user_data and verify(password,user_data[2]) else None

def create_token(user_data: tuple) -> str:
    return f'{user_data[0]}{_SEPARATOR}{user_data[1]}'


def is_authenticated(token: str) -> bool:
    id_user, email = token.split(_SEPARATOR)
    
    user_data = fetchOne_query('''SELECT id_user,email,password,role 
    FROM user WHERE id_user = ?''', (id_user,))

    authenticated = False
    if user_data:
        if email == user_data[1]:
            authenticated = True

    return authenticated


def from_token(token: str) -> User | None:
    pass
    # _, username = token.split(_SEPARATOR)

    # return find_by_username(username)

def create(email: str, password: str) -> User | None:
    hashed_password = hash(password) #hashed_password = hashing.pwd_context.hash(data.password)
    password = hashed_password

    try:
        generated_id = insert_query(
            'INSERT INTO user(email, password, role) VALUES (?,?,?)',
            (email, password, 'user'))

        return User(id=generated_id, email=email, password='', role='user')
    
    except IntegrityError:
        # mariadb raises this error when a constraint is violated
        # in that case we have duplicate usernames
        return None
    
def user_registered(email:str):
    user_data = fetchOne_query('''SELECT email,password,role 
    FROM user WHERE email = ?''', (email,))

    if user_data:
        return True
    else:
        return False
    

def promote_to_director_request_exists(id_user:int):
    request_data = fetchOne_query('''SELECT id_dir_req
    FROM director_requests WHERE id_user = ?''', (id_user,))

    return True if request_data else False

def director_promote_request_send(id_user):
    # id_request = fetchOne_query('''SELECT id_dir_req
    # FROM director_requests WHERE id_user = ?''', (id_user,))
    # return id_request

    try:
        generated_id = insert_query(
            'INSERT INTO director_requests(id_user) VALUES (?)',
            (id_user,))
        return generated_id

    except IntegrityError:
        # mariadb raises this error when a constraint is violated
        # in that case we have duplicate usernames
        return None

def user_and_player_already_linked(id_user: int, player_name: str):
    id_player = find_player_id_by_name(player_name)
    if id_player:
        approved_link = fetchOne_query('''SELECT approval 
        FROM user_has_player 
        WHERE (user_id_user=? AND player_id_player)''', (id_user,id_player))
        return approved_link
    
    return False

def link_profile_request_exists():
    '''
    Function validates if link request already exists for this
    user. Returns true if it does
    '''
    pass

def find_player_id_by_name(player_name:str):
    player_id = fetchOne_query('''SELECT id_player 
    FROM player WHERE full_name = player_name''', (player_name,))

    return player_id

def is_admin(token:str):
    id_admin, email = token.split(_SEPARATOR)
    # print(id_admin)
    # print(email)
    user_role = fetchOne_query('''SELECT role 
    FROM user WHERE id_user = ?''', (id_admin,))
    
    # print(user_role[0])
    # print(email)
    return True if (user_role[0]=='admin' and email=='admin@gmail.com') else False

def valid_admin_decision(admin_decision:str):
    return (admin_decision=='accept' or admin_decision=='decline')

def accept_or_decline_promote_to_director_request(admin_decision,
                                                  id_request,
                                                  id_user):
    '''Deletes requests which are declined and moves the 
    accepted request in approval column in user_has_player table
    and then deletes request'''


    if admin_decision == 'decline':
        update_query('''DELETE FROM director_requests
                    WHERE id_dir_req = ?''', (id_request,))
        return 'Declined'
    
    elif admin_decision=='accept':
        update_query('''UPDATE user
                        SET role = "director"
                        WHERE id_user = ?''', (id_user,))

        update_query('''DELETE FROM director_requests
                    WHERE id_dir_req = ?''', (id_request,))
    
        return 'Accepted'

def user_already_director(id_user):
    user_role = fetchOne_query('''SELECT role 
                                FROM user WHERE id_user = ?''',
                                (id_user,))
    
    return True if user_role[0] == 'director' else False
