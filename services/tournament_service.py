from data.database import fetchOne_query, insert_query, read_query, update_query,count_query
from pydantic import BaseModel, EmailStr, constr
from data.models import MatchForGetTour, NextRoundInfo, ReturnTournament, Tournament, CreateTournament, Match
import datetime
from datetime import timedelta
import random
from mariadb import IntegrityError
from utils.hashing import hash,verify

_SEPARATOR = ';;;token;;;'


def create_tournament(tournament:CreateTournament):
    #create tournament in database
    tournament_id = insert_query('''INSERT INTO 
    tournament(title,format,match_format) VALUES (?,?,?)''',
                (tournament.title,tournament.tournament_format,
                 tournament.match_format))
    
    if tournament.tournament_format =='league':
        matches = generate_matches(
                                tournament.participants,
                               tournament.match_format,
                               tournament_id)
        

    elif tournament.tournament_format == 'knockout':
        matches = generate_matches_knockout(
                                tournament.participants,
                               tournament.match_format,
                               tournament_id)


    return tournament_id,matches

def players_exist(participants,country):
    '''Checks if a player already exists in the database and if 
        not adds the player to it'''
    for name in participants:
        #check if its in player   
        user_data = fetchOne_query('''SELECT id_player 
        FROM player WHERE full_name = ?''', (name,))

        if not user_data:
            add_player_to_database(name,country)

def add_player_to_database(name,country):
    generated_id = insert_query(
            'INSERT INTO player(full_name,country) VALUES (?,?)',
            (name,country))

def tournament_exists(tour_name:str):
    tour_data = fetchOne_query('''SELECT title 
        FROM tournament WHERE title = ?''', (tour_name,))

    return True if tour_data else False

#match
def calculate_time_of_play_of_match():
    '''Gives a time of play of the match. It is between 
        The earliest start date is one month after the day
        the tournament was created. The last match can be played
        30 days after the start date.'''
    start_date = datetime.datetime.now()
    start_date = start_date + timedelta(days=30)

    end_date = start_date + timedelta(days=30)

    random_date = start_date + (end_date - start_date) * random.random()
    final_date = random_date.replace(hour=17,minute=30)

    return final_date

#create match

def generate_matches(players,match_format,tournament_id): #league
    matches = []
    for i in range(len(players)):
        # print(i)
        for j in range(i + 1, len(players)):
            # print(j)
            time_of_play = calculate_time_of_play_of_match()
            # print(time_of_play)
            match = Match(players[i], players[j],time_of_play,match_format)
            # print(match)
            matches.append(match)
            match_id = insert_match_to_database(time_of_play,match_format,
                                                players[i],players[j]) #30min

            insert_data_to_match_has_tournament(match_id,tournament_id)

    return matches

def insert_match_to_database(time_of_play,match_format,player1
                             ,player2):
    generated_id = insert_query(
                '''INSERT INTO 
                match_score.match(time_of_play,match_format,player1,player2) 
                VALUES (?,?,?,?)''', #1h
                (time_of_play,match_format,player1,player2))
    
    # generated_id = insert_query(
    #             'INSERT INTO match(time_of_play,match_format) VALUES (?,?)',
    #             (time_of_play,match_format))

    return generated_id

def generate_matches_knockout(players,match_format,tournament_id):
    matches = []
    for i in range(0, len(players), 2):
        time_of_play = calculate_time_of_play_of_match()

        if i == len(players)-1:
            id_pl = get_player_id(players[i])
            insert_round_knockout_t(id_pl,tournament_id,2)
            break

        id_pl = get_player_id(players[i])
        id_pl_2 = get_player_id(players[i+1])

        insert_round_knockout_t(id_pl,tournament_id,1)
        insert_round_knockout_t(id_pl_2,tournament_id,1)

        match = Match(players[i],players[i+1],time_of_play,match_format)
        matches.append(match)
        match_id = insert_match_to_database_knockout(time_of_play,match_format,
                                                players[i],players[i+1])
        
        insert_data_to_match_has_tournament(match_id,tournament_id)

    return matches

def update_matches_knockout_next_round(players,match_format,tournament_id,c_r):
    matches = []
    # ids_players = []
    # for name in players:
    #     id_player = fetchOne_query('''SELECT id_player 
    #                                 FROM player
    #                                 WHERE full_name = ?''',(name,))
    #     ids_players.append(name)

    # players = ids_players.copy()
    print("--------")
    print(players)
    print("-------")
    if players == None:
        return None
    elif len(players)==2:
        i=0
        id_pl = get_player_id(players[0])
        update_knockout_t(id_pl,tournament_id,c_r+1)

        time_of_play = calculate_time_of_play_of_match()

        match = Match(players[i],players[i+1],time_of_play,match_format)
        matches.append(match)
        data =[match.player1,match.player2,match.time_of_play,match.match_format]
        match_id = insert_match_to_database_knockout(time_of_play,match_format,
                                                    players[i],players[i+1])
        
        insert_data_to_match_has_tournament(match_id,tournament_id)
        print(match.player2,match.player1)
        return 2,data
    else:
        for i in range(0, len(players), 2):
            time_of_play = calculate_time_of_play_of_match()

            id_pl = get_player_id(players[i])
            id_pl_2 = get_player_id(players[i+1])

            insert_round_knockout_t(id_pl,tournament_id,c_r)
            insert_round_knockout_t(id_pl_2,tournament_id,c_r)

            match = Match(players[i],players[i+1],time_of_play,match_format)
            matches.append(match)
            match_id = insert_match_to_database_knockout(time_of_play,match_format,
                                                    players[i],players[i+1])
            
            insert_data_to_match_has_tournament(match_id,tournament_id)

    return matches

def insert_match_to_database_knockout(time_of_play,match_format,player1
                             ,player2,round=1):
    generated_id = insert_query(
                '''INSERT INTO 
                match_score.match(time_of_play,match_format,player1,player2,round) 
                VALUES (?,?,?,?,?)''', #1h
                (time_of_play,match_format,player1,player2,round))

    return generated_id


def insert_data_to_match_has_tournament(match_id,tournament_id):
    insert_query(
                '''INSERT INTO match_has_tournament
                (match_id_match,tournament_id_tournament)
                  VALUES (?,?)''',
                (match_id,tournament_id))
    
def token_split(token:str) -> tuple:
    id_user, email = token.split(_SEPARATOR)
    
    return id_user,email

def view_tournament(id_tour:int):
    tour_data = fetchOne_query('''SELECT title,format 
    FROM tournament WHERE id_tournament = ?''', (id_tour,))

    id_matches = fetchOne_query('''SELECT match_id_match
    FROM match_has_tournament WHERE tournament_id_tournament = ?''', (id_tour,))

    # print(id_matches)
    # print('------------------------------')

    id_matches = list(id_matches)
    i = id_matches[0]
    while True:
        curr_match_id = fetchOne_query('''
        SELECT match_id_match
        FROM match_has_tournament 
        WHERE tournament_id_tournament = ? AND match_id_match > ?''', (id_tour, i))  
        if curr_match_id==None:
            break
        i+=1
        id_matches.append(curr_match_id[0])

    t_matches_fin = []

    for id in id_matches:
        match_data = fetchOne_query('''
        SELECT time_of_play,match_format,player1,player2,score1,score2
        FROM match_score.match WHERE id_match = ?''', (id,))
        print(match_data)

        match = MatchForGetTour(match_data[0],match_data[1],
                      match_data[2],match_data[3],
                      match_data[4],match_data[5])

        t_matches_fin.append(match)

    tournament_view= ReturnTournament(tour_data[0],tour_data[1],[t_matches_fin])    

    return tournament_view
    #return tour_data,tour_data_2


def check_all_matches_played(id_tour:int):
    '''Checks if all matches in the tournament have 
    been played in order to create the next round of the match'''
    tour_data = fetchOne_query('''SELECT title,format 
    FROM tournament WHERE id_tournament = ?''', (id_tour,))

    id_matches = fetchOne_query('''SELECT match_id_match
    FROM match_has_tournament WHERE tournament_id_tournament = ?''', (id_tour,))

    id_matches = list(id_matches)
    i = id_matches[0]

    while True:
        curr_match_id = fetchOne_query('''
        SELECT match_id_match
        FROM match_has_tournament 
        WHERE tournament_id_tournament = ? AND match_id_match > ?''', (id_tour, i))  
        if curr_match_id==None:
            break
        i+=1
        id_matches.append(curr_match_id[0])

    t_matches_fin = []

    for id in id_matches:
        match_data = fetchOne_query('''
        SELECT time_of_play,match_format,player1,player2,score1,score2
        FROM match_score.match WHERE id_match = ?''', (id,))

        score1 = match_data[4]
        score2 = match_data[5]
        if score1==None or score2==None:

            return id
        
    return None

def type_of_tournament(id:int):
    '''Gets the type of a tournament with a certain id and returns it'''

    data = fetchOne_query('''
        SELECT format
        FROM tournament WHERE id_tournament = ?''', (id,))
    return data[0]

def next_round_knockout(id:int):
    pass


def give_back_winner_of_match(id_m:int):
    winner = fetchOne_query('''
                            SELECT
                                CASE 
                                    WHEN score1 > score2 THEN player1
                                    WHEN score2 > score1 THEN player2
                                    ELSE 'Both players have the same score'
                                END AS player_with_higher_score
                            FROM 
                                match_score.match
                            WHERE
                                id_match=?;''',(id_m,))
    
    return winner[0]

def give_winners_of_matches(info:NextRoundInfo):
    id_t = info.id_t
    ids_matches = info.ids_matches
    c_r = info.curr_round

    win_players = []
    #iterate the matches. add to list
    for id_m in ids_matches:
        win_pl = give_back_winner_of_match(id_m)
        print(win_pl)
        win_players.append(win_pl)

    #add player from next round
    lucky_pl = select_player_from_next_round_for_next_round(c_r,id_t)
    if lucky_pl:
        print(lucky_pl)
        win_players.append(lucky_pl)
    print(win_players)
    return win_players   
    
def set_round(id_m:int):
    pass

def insert_round_knockout_t(id_pl:int,id_tou:int,round):
    id_knockout= insert_query(
                '''INSERT INTO knockout_t
                (player_id_player,tournament_id_tournament,round)
                  VALUES (?,?,?)''',
                (id_pl,id_tou,round))
    
    return id_knockout

def get_player_id(name:str):
    player_data = fetchOne_query('''SELECT id_player 
        FROM player WHERE full_name = ?''', (name,))
    
    return player_data[0]

def select_player_from_next_round_for_next_round(c_r:int,id_t):
    id_lucky_pl = fetchOne_query('''
                                SELECT player_id_player
                                FROM knockout_t
                                WHERE round=? AND 
                                tournament_id_tournament=?''',(c_r+1,id_t))
    
    player_name = fetchOne_query('''
                                SELECT full_name
                                FROM player
                                WHERE id_player = ?''',(id_lucky_pl,))
    
    if player_name[0]:
        return player_name[0]
    else:
        None

def update_knockout_t(id_pl,id_t,c_r):
    data = update_query('''UPDATE knockout_t 
                            SET round = ?
                            WHERE player_id_player=? AND 
                                tournament_id_tournament=?''',
                                (c_r,id_pl,id_t))
    

def get_match_format_by_id(id_m:int):
    match_type = fetchOne_query('''SELECT match_format
                                    FROM match_score.match
                                    WHERE id_match = ?''',(id_m,))
    

    return match_type[0]