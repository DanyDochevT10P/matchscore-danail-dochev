from fastapi import FastAPI
from routers.user import user_router
from routers.tournament import tournament_router
from routers.match import match_router

app = FastAPI()
app.include_router(user_router)
app.include_router(tournament_router)
app.include_router(match_router)