import datetime
from datetime import timedelta
import random

from data.database import fetchOne_query
# start_date = datetime.datetime.now()
# start_date = start_date + timedelta(days=30)

# end_date = start_date + timedelta(days=30)

# random_date = start_date + (end_date - start_date) * random.random()
# final_date = random_date.replace(hour=17,minute=30)

# print(final_date)

# def give_back_winner_of_match(id_m:int):
#     winner = fetchOne_query('''
#                             SELECT 
#                                 CASE 
#                                     WHEN score1 > score2 THEN player1
#                                     WHEN score2 > score1 THEN player2
#                                     ELSE 'Both players have the same score'
#                                 END AS player_with_higher_score
#                             FROM 
#                                 match_score.match
#                             WHERE
#                                 id_match=?;''',(id_m,))
    
#     print(winner)


# give_back_winner_of_match(52)


# title = "Turkey Cup Championship"

# country = title.split(' ')[0]
# print(country)


def select_player_from_next_round_for_next_round(c_r:int,id_t):
    id_lucky_pl = fetchOne_query('''
                                SELECT player_id_player
                                FROM knockout_t
                                WHERE round=? AND 
                                tournament_id_tournament=?''',(c_r+1,id_t))
    
    player_name = fetchOne_query('''
                                SELECT full_name
                                FROM player
                                WHERE id_player = ?''',id_lucky_pl)
    
    return player_name

pl_n = select_player_from_next_round_for_next_round(1,38)

print(pl_n)