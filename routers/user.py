from fastapi import APIRouter, Header
from common.auth import get_user_or_raise_401
from common.responses import BadRequest, Unauthorized, NotFound,NoContent,InternalServerError
from data.models import LoginData,User,UserLogIn,PromoteToDirectorRequest
from services.user_service import create, is_admin,try_login,is_authenticated,promote_to_director_request_exists,director_promote_request_send, user_and_player_already_linked, user_registered, valid_admin_decision,accept_or_decline_promote_to_director_request,user_already_director
from services import user_service,tournament_service
from pydantic import EmailStr
from utils.hashing import hash,verify


user_router = APIRouter(prefix='/user')

@user_router.post('/register')
def register_user(data: LoginData):
    # check if email already exists
    
    if user_registered(data.email):
        return BadRequest('Email adress already used!')
    else:
        new_user = user_service.create(data.email,data.password)
        return new_user

@user_router.post('/login')
def login(data: LoginData):

    user_data = user_service.try_login(data.email, data.password)
    # print(user_data)
    if user_data:
        token = user_service.create_token(user_data)
        return {'token': token}
    else:
        return BadRequest('Invalid login data')

@user_router.post('/request/{id}/director') #id is id of user
def promote_to_director_request(id:int, token: str = Header()):
    if not is_authenticated(token):
        return Unauthorized('Not authorized')
    
    #check if request already exists
    if promote_to_director_request_exists(id):
        return BadRequest('Request for director promotion already sent')

    #check if user is already director


    return {'Promote-to-director request id':
            director_promote_request_send(id)}

@user_router.post('/request/{id}/association')
def associate_with_player_request(id:int, player_name:str 
                                  ,token:str = Header()):
    id_user = id

    if user_and_player_already_linked(id_user,player_name):
        return(BadRequest('Logged in user and player already linked'))
    
    #if link_profile_request_exists -> return BadRequest

    if not is_authenticated(token):
        return Unauthorized('Not authorized')
    
    
@user_router.put('/request/director')
def evaluate_promote_to_director_request(data:PromoteToDirectorRequest,
                                         token:str=Header()):
    # print('here')
    
    if not is_admin(token):
        return BadRequest('''Only admins can evaluate 
        promote-to director requests''')

    if not valid_admin_decision(data.admin_decision):
        return BadRequest('''Promote-to director requests
        can only be accepted or declined. Please enter a 
        valid command''') 
    
    if user_already_director(data.id_user):
        return BadRequest('''User is already a director.''') 
    
    final_answer = accept_or_decline_promote_to_director_request(
                                                 data.admin_decision,
                                                data.id_request,
                                                data.id_user)

    return {'Status of request': final_answer}