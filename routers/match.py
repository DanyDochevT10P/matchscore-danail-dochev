from fastapi import APIRouter, Header
from common.auth import get_user_or_raise_401
from common.responses import BadRequest, Unauthorized, NotFound,NoContent,InternalServerError
from http.client import HTTPResponse
from fastapi import APIRouter, HTTPException, status, Depends,Response
from data.database import fetchOne_query, read_query, insert_query, update_query
from data.models import CreateTournament, Score
from services.tournament_service import create_tournament, players_exist, token_split, tournament_exists,_SEPARATOR
from services.user_service import _SEPARATOR, is_admin, user_already_director
from services.match_service import change_match_score, return_all_matches,return_match_by_id
import datetime
from datetime import timedelta
import random

match_router = APIRouter(prefix='/match')

@match_router.put('/score/{id}') #id is id of match
def set_match_score(id:int,score: Score, token:str=Header()):
    score1 = score.score1
    score2 = score.score2
    id_user, email = token.split(_SEPARATOR)

    if is_admin(token)==False and user_already_director(id_user)==False:
        return Unauthorized('''Only admins and directors can edit score in matches''')
    
    result = change_match_score(score1,score2,id)
    
    return {'''result updated successfuly with score:''': result}
    #check if all matches in this tournament have been played
    #if yes make next round
    #if knockout: if team only one then return winner is

    #if league then see who has most points and also decide how many matches
    #are to be played
    pass

@match_router.get('/{id}')
def get_match_by_id(id:int):
    match_data = return_match_by_id(id)

    return {'time of play:': match_data[0],
            'match format': match_data[1],
            'player1': match_data[2],
            'player2':match_data[3]}

#with the option to sort descending and ascending
# @match_router.get('/')
# def get_all_matches():
#     pass