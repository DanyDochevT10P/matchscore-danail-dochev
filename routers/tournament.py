from ast import Match
from fastapi import APIRouter, Header
from common.auth import get_user_or_raise_401
from common.responses import BadRequest, Unauthorized, NotFound,NoContent,InternalServerError
from http.client import HTTPResponse
from fastapi import APIRouter, HTTPException, status, Depends,Response
from data.database import fetchOne_query, read_query, insert_query, update_query
from data.models import CreateTournament, NextRoundInfo
from services.tournament_service import check_all_matches_played, create_tournament, get_match_format_by_id, give_winners_of_matches, players_exist, token_split, tournament_exists,_SEPARATOR, view_tournament,type_of_tournament,generate_matches_knockout,update_matches_knockout_next_round
from services.user_service import _SEPARATOR, is_admin, user_already_director
import datetime
from datetime import timedelta
import random


tournament_router = APIRouter(prefix='/tournament')

@tournament_router.post('/create')
def create_new_tournament(tournament:CreateTournament,token:str=Header()):
    
    #check if user is director or admin
    title = tournament.title
    country = title.split(' ')[0]

    # print(token)
    id_user= token.split(_SEPARATOR)[0] #3h
    # print(id_user)
    # print(email)
    # print('The code is getting here too')
    # print(token)

    if is_admin(token)==False and user_already_director(id_user)==False:
        return Unauthorized('''Only admins and directors can create tournaments''')
    
    #check if tournament exists in db

    if tournament_exists(tournament.title):
        return BadRequest('''Tournament with this name 
        already exists''')

    #check tournament format
    if (tournament.tournament_format!='knockout' and tournament.tournament_format!='league'):
        return BadRequest('''Only 'knockout and 'league'
        are valid tournament formats''')

    #create tournament
    #insert players to db if not there already
    players_exist(tournament.participants,country)

    id_tour_and_matches = create_tournament(tournament)
    
    return {'''Tournament was created with the following id and matches''': id_tour_and_matches}
  

@tournament_router.get('/{id}')
def view_tournament_by_id(id:int):
    
    result = view_tournament(id)

    return {"tournament data": result}

#next_round
@tournament_router.post('/{id}/knockout/round/next')
def make_next_round_for_knockout(info:NextRoundInfo,token:str=Header()):

    id_user = token.split(_SEPARATOR)[0]

    if is_admin(token)==False and user_already_director(id_user)==False:
        return Unauthorized('''Only admins and directors can create tournaments''')

    # result = check_all_matches_played(info.id_t)
    # # if result:    
    # #     return BadRequest(f'Match with id: {result} has not yet been played!')
    
    players = give_winners_of_matches(info)
    match_format = get_match_format_by_id(info.ids_matches[0])

    res = update_matches_knockout_next_round(players,match_format,info.id_t,info.curr_round)
    if res == None:
        return "End of tournament, hope you had great time!"


    if res[0]==2:
        return {"Finals match":"!!!",
                "Player 1": res[1][0],
                "Player 2":res[1][1],
                "Time of play":res[1][2],
                "Match format":res[1][3]}
    elif len(res)>1:
        return {"Next round matches:": res}
    
    #make next round
